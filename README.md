mc102
===========

Introduction to Algorithms exercises from Unicamp.

I plan to play around with new languages doing those simple exercises.  As the
intent is learning please do not take as reference any code that you might find
here :)

Many thanks to Zanoni Dias!
http://www.ic.unicamp.br/~zanoni/
